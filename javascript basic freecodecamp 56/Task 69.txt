function testLogicalOr(val) {
  // Only change code below this line

  if (val > 20 || val <= 9) {
    return "Outside";
  }

  // Only change code above this line
  return "Inside";
}

testLogicalOr(20);
